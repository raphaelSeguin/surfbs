import React, { Component, useEffect, useState } from 'react';
import Main from './Components/Main'
import Auth from './Components/Auth'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/auth">
          <Auth/>
        </Route>
        <Route path="/">
          <Main/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
