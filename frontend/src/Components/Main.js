import React, { Component, useEffect, useState } from 'react';
import Utilisateurs from './Utilisateurs'
import Commentaires from './Commentaires'
import Sujets from './Sujets'

const Main = () => {
  return <div className="App">
    <header className="App-header">
      surfbs
    </header>
    <div className="Corps">
      <Utilisateurs/>
      <Commentaires/>
      <Sujets/>
    </div>
  </div>
}


export default Main