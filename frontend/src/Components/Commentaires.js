import React, { useEffect, useState } from 'react';

const Commentaires = () => {
  const [commentaires, setCommentaires] = useState([])
  let notifyCounter = 0;
  useEffect( () => {
    fetch('/commentaires')
      .then( res => {
        if (res.ok) {
          return res.json()
        }
      })
      .then( array => {
        console.log(array)
        setCommentaires(array)
      })
      .catch( err => console.log(err))
  }, [])
  
  const notify = () => {
    notifyCounter += 1
    console.log(notifyCounter, 'pouet')
  }
  return <div className="center bg-commentaire flexcol">
    <div className="Commentaires">
      <ul>
        {
          commentaires.map( (u, n) => {
            return <li key={n}>
              <Com auteur={u.auteur} texte={u.texte}/>
            </li>
          })
        }
      </ul>
    </div>
    <Ecrire notify={notify}/>
  </div>
}

const Ecrire = ({notify}) => {
  const [textInput, setTextvalue] = useState('')
  return <div className="Ecrire">
    <textarea value={textInput} onChange={(c) => setTextvalue(c.target.value)}/>
    <button
      onClick={() => {
        postComment({auteur: 'anon.', texte: textInput})
          .then( response => console.log(response))
          //.then( () => notify())
      }}
    >commenter</button>
  </div>
}

const Com = ({auteur, texte}) => {
  return <p>
    <span className="bold">{auteur}: </span>{texte}
  </p>
}


/* ACTION */
const postComment = ({auteur, texte}) => {
  return fetch('/commentaire', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ auteur, texte })
  })
}

export default Commentaires