import React, { useEffect, useState } from 'react';


const Sujets = () => {
  const [sujets, setSujets] = useState([])
  useEffect( () => {
    fetch('/sujets')
      .then( res => {
        if (res.ok) {
          return res.json()
        }
      })
      .then( array => {
        console.log('sujets: ', array)
        setSujets(array)
      })
  }, [])
  return <div className="right bg-sujet">
    <ul>
      {
        sujets.map( (s) => {
          return <li key={s}>
            {s}
          </li>
        })
      }
    </ul>
  </div>
}

export default Sujets