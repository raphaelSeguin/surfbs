import React, { useEffect, useState } from 'react';

const Utilisateurs = () => {
  const [ utilisateurs, setUtilisateurs ] = useState([]);
  useEffect( () => {
    fetch('/utilisateurs')
      .then( res => {
        if (res.ok) {
          return res.json()
        }
      })
      .then( array => {
        console.log(array)
        setUtilisateurs(array)
      })
  }, [])
  return <div className="left bg-utilisateur">
    <ul>
      {
        utilisateurs.map( (u, n) => {
          return <li key={n}>
            {u}
          </li>
        })
      }
    </ul>
  </div>
}

export default Utilisateurs