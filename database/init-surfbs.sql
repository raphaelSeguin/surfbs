CREATE USER surfbs WITH PASSWORD 'surfbs';
GRANT ALL PRIVILEGES ON DATABASE surfbs TO surfbs;
\c sufbs
CREATE TABLE utilisateur (
        alias VARCHAR(50) UNIQUE NOT NULL
);
CREATE TABLE commentaire (
        texte VARCHAR(1000),
        auteur VARCHAR(50)
);
CREATE TABLE sujet (
  sujet VARCHAR(100),
);
GRANT ALL PRIVILEGES ON TABLE utilisateur, commentaire TO surfbs;