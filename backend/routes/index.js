const express = require('express');
const router = express.Router();
const pool = require('../db')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/utilisateurs', (req, res) => {
  pool.query('SELECT * FROM utilisateur', (err, results) => {
    if (err) return res.status(500).send('db error')
    res.status(200).json(results.rows.map( u => u.alias))
  })
})

router.get('/commentaires', (req, res) => {
  pool.query('SELECT * FROM commentaire', (err, results) => {
    if (err) return res.status(500).send('db error')
    res.status(200).json(results.rows)
  })
})

router.get('/sujets', (req, res) => {
  pool.query('SELECT * FROM sujet', (err, results) => {
    if (err) return res.status(500).send('db error')
    res.status(200).json(results.rows.map( s => s["sujet"]))
  })
})

router.post('/commentaire', (req, res) => {
  const { auteur, texte } = req.body
  if (auteur && texte) {
    pool.query(`INSERT INTO commentaire (texte, auteur) VALUES ('${texte}', '${auteur}')`, (err, results) => {
      if (err) return res.status(500).send('db error')
      res.status(200).send()
    })
  } else {
    res.status(500).send('incomplet: auteur et texte requis')
  }
})

module.exports = router;
